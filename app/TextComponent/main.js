/* jshint esnext: true */

'use strict'; // not needed in es6 module but jshint shows error message --> is there a work-around?
			  // Error message: missing "use strict" statement

import $ from 'jquery';

const textArray = [
	'Click me to change!',
	'jQuery is working',
	'I\'m the last in the list'];

const ANIMATION = 'pulse';

class TextComponent {
	constructor(element) {
		/*var _curPos = 0,
			_curText = ''; // private members but how do I create setters / getters for them
						   // get curPos won't work 
						   // --> only method in constructor can access them but not with 
						   //     real setters or getters.
		*/

		//init.call(this);

		(() => {
			init.call(this); // why is call this required?!
			this.addHandlers();
		})();

		//this.updateDOM.call(this); // call(this) is done with arrow function above
		//this.addHandlers.call(this);

		// private methods
		function init() {
			/* jshint validthis: true */
			console.log('init called!!');
			var item = getRandomItem();
			// console.log($(element), 'init element!!!');
			this._$el = $(element);  // this._$el is not private, is it possible to make it private?

			// add animation class to element
			this._$el.addClass('animated');

			this._curText = '';
			this._curPos = textArray.indexOf(item);
			this._curText = item;
		}

		function getRandomItem() {
			return textArray[Math.floor(Math.random() * textArray.length)];
		}

		// return public API 
		// --> is there a ES6 short form of this?
		// --> is the same functionality also possible with-out $.extend(...)?
		// --> how can I make _$el, _curText,... really private with setters/getters?

		// $.extend works but there is an ES6 way...
		// return $.extend(this, {
		// 		getRandomItem: getRandomItem // make private method getRandomItem public
		// 	});

		return Object.assign(this, {
			getRandomItem: getRandomItem // make private method getRandomItem public
		});
	}

	set curText(text) {
		this._curText = text;
		this.updateDOM();
	}

	get curText() {
		return this._curText;
	}

	set $el(el) {
		this._$el = el;
	}

	get $el() {
		return this._$el;
	}

	updateDOM() {
		console.log(this.curText, this,  'updated!!!');
		this.$el.text(this.curText); // this.curTextis the getter of curText
	}

	addHandlers() {
		//console.log(this, 'added handlers');
		var $el = this.$el;

		this.handler = {
			'click': () => {
				//console.log('clicked', this);

				this._curPos = (++this._curPos < textArray.length) ? this._curPos : 0;

				//console.log(this._curPos);
				this._curText = textArray[this._curPos];
				this.updateDOM();
			},
			'mouseenter mouseleave': (event) => {
				console.log('hovered', event);
				//$el.toggleClass('bounce');
				(event.type === 'mouseenter') ? $el.addClass(ANIMATION): $el.removeClass(ANIMATION);
			},
			'transitionend webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd': (event) => {
				// not working yet --> gets not triggered
				console.log('animation ended', event);
			}
		};

		$el.on(this.handler);
	}
}

export default TextComponent;