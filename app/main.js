// main.js

import TextComponent from './TextComponent/main';

console.log(TextComponent);
var text = new TextComponent('#heading');

// example usage of public api
console.log('mainjs: textComponent obj. - ', text);

console.log('mainjs: random item from textArray - ', text.getRandomItem());

text.curText = 'Hello from mainjs.... Click here!!';

console.log('mainjs: currently set text - ', text.curText);